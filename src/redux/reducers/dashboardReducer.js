const initialState = {  
  log: '23',
  startDate: '',
  endDate: ''
};

export default function (state = initialState, action) {
  switch (action.type) {
    case 'DASHBOARD_ACTION':
      return { ...state, log: action.payload };
    case 'START_DATE':
      return { ...state, startDate: action.payload };
    case 'END_DATE':
      return { ...state, endDate: action.payload };
    default:
      return state;
  }
}


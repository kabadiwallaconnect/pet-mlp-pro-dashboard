const initialState = {  
  cost: {},
  costLoading: false 
};

export default function (state = initialState, action) {
  switch (action.type) {
    case 'COST':
      return { ...state, costLoading: true };
    case 'COST_SUCCESS':
      return { ...state, cost: action.payload, costLoading: false };
    case 'COST_FAIL':
      return { ...state, costLoading: false };
    default:
      return state;
  }
}

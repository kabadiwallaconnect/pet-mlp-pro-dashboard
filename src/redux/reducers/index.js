import themeReducer from './themeReducer';
import sidebarReducer from './sidebarReducer';
import dashboardReducer from './dashboardReducer';
import volumeReducer from './volumeReducer';
import costReducer from './costReducer';
import supplierReducer from './supplierReducer';

export {
  themeReducer,
  sidebarReducer,
  dashboardReducer,
  volumeReducer,
  costReducer,
  supplierReducer
};

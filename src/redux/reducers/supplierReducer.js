const initialState = {  
  supplierMap: '',
  supplierMapLoading: false,
  suppliers: '',
  category: '',
  suppliersLoading: false,
  supplierPrice: '',
  supplierPriceLoading: false,
  supplierList: [],
  supplierListL1: [],
  supplierListL2: [],
  supplierListLoading: false,
  supplierListL1Loading: false,
  supplierListL2Loading: false,
  allOrdersLoading: false,
  orderDetailsLoading: false,
  currentPage: 1,
  allPages: [1, 2, 3, 4, 5, 6, 7, 8],
  topOrders: [],
  allOrders: [],
  orderDetails: {}
};

export default function (state = initialState, action) {
  switch (action.type) {
    case 'PAGES':
      return { ...state, allPages: action.payload };
    case 'CURRENT_PAGE':
      return { ...state, currentPage: action.payload };
    case 'CATEGORY_STORE':
      return { ...state, category: action.payload };
      
    case 'SUPPLIER_MAP':
      return { ...state, supplierMapLoading: true };
    case 'SUPPLIER_MAP_SUCCESS':
      return { ...state, supplierMap: action.payload, supplierMapLoading: false };
    case 'SUPPLIER_MAP_FAIL':
      return { ...state, supplierMapLoading: false };  
      
    case 'SUPPLIERS':
      return { ...state, suppliersLoading: false };
    case 'SUPPLIERS_SUCCESS':
      return { ...state, suppliers: action.payload, suppliersLoading: false };
    case 'SUPPLIERS_FAIL':
      return { ...state, suppliersLoading: false };
      
    case 'SUPPLIER_PRICE':
      return { ...state, supplierPriceLoading: false };
    case 'SUPPLIER_PRICE_SUCCESS':
      return { ...state, supplierPrice: action.payload, supplierPriceLoading: false };
    case 'SUPPLIER_PRICE_FAIL':
      return { ...state, supplierPriceLoading: false };
      
    case 'SUPPLIER_LIST':
      return { ...state, supplierListLoading: true };
    case 'SUPPLIER_LIST_SUCCESS':
      return { ...state, supplierList: action.payload, supplierListLoading: false };
    case 'SUPPLIER_LIST_FAIL':
      return { ...state, supplierListLoading: false };
    
    case 'SUPPLIER_LIST_L1':
      return { ...state, supplierListL1Loading: true };
    case 'SUPPLIER_LIST_L1_SUCCESS':
      return { ...state, supplierListL1: action.payload, supplierListL1Loading: false };
    case 'SUPPLIER_LIST_L1_FAIL':
      return { ...state, supplierListL1Loading: false };
    
    case 'SUPPLIER_LIST_L2':
      return { ...state, supplierListL2Loading: true };
    case 'SUPPLIER_LIST_L2_SUCCESS':
      return { ...state, supplierListL2: action.payload, supplierListL2Loading: false };
    case 'SUPPLIER_LIST_L2_FAIL':
      return { ...state, supplierListL2Loading: false };
      
    case 'GET_TOP_ORDERS':
      return { ...state, topOrdersLoading: true };
    case 'GET_TOP_ORDERS_SUCCESS':
      return { ...state, topOrders: action.payload, topOrdersLoading: false };
    case 'GET_TOP_ORDERS_FAIL':
      return { ...state, allOrdersLoading: false };
        
    case 'GET_ALL_ORDERS':
      return { ...state, allOrdersLoading: true };
    case 'GET_ALL_ORDERS_SUCCESS':
      return { ...state, allOrders: action.payload, allOrdersLoading: false };
    case 'GET_ALL_ORDERS_FAIL':
      return { ...state, allOrdersLoading: false };
      
    case 'GET_ORDER_DETAILS':
      return { ...state, orderDetailsLoading: true };
    case 'GET_ORDER_DETAILS_SUCCESS':
      return { ...state, orderDetails: action.payload, orderDetailsLoading: false };
    case 'GET_ORDER_DETAILS_FAIL':
      return { ...state, orderDetailsLoading: false };
        
    default:
      return state;
  }
}

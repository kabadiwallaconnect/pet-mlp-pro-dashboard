const initialState = {  
  volume: {},
  volumeLoading: false 
};

export default function (state = initialState, action) {
  switch (action.type) {
    case 'VOLUME':
      return { ...state, volumeLoading: true };
    case 'VOLUME_SUCCESS':
      return { ...state, volume: action.payload, volumeLoading: false };
    case 'VOLUME_FAIL':
      return { ...state, volumeLoading: false };
    default:
      return state;
  }
}

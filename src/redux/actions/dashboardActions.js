export const dashboardAction = () => {
  console.log('action');
  return {
    type: 'DASHBOARD_ACTION',
    payload: "value"
  };
};

export const storeStartDate = (startDate) => {
  const formattedDate = startDate.format('YYYY-MM-DD').toString();
  console.log('formattedStartDate', formattedDate);
  return {
    type: 'START_DATE',
    payload: formattedDate
  };
};

export const storeEndDate = (endDate) => {
  const formattedDate = endDate.format('YYYY-MM-DD').toString();
  console.log('formattedStartDate', formattedDate);
  return {
    type: 'END_DATE',
    payload: formattedDate
  };
};

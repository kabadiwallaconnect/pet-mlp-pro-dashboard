import axios from 'axios';

const getCost = ({ startVal, endVal, materialType }) => {
  return (dispatch) => {
    dispatch({ type: 'COST' });
    axios.get('https://pro-api.kcdev.xyz/public/costs', {
      params: {
        start: startVal,
        end: endVal,
        material: materialType
      }
    }).then(res => {
      dispatch({ type: 'COST_SUCCESS', payload: res.data });
    }).catch(err => {
      console.log(err);
      dispatch({ type: 'COST_FAIL' });
    });
  };
};

export default getCost;

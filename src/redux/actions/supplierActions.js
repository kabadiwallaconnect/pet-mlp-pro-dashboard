import axios from 'axios';

export const pageStore = (pages) => {
  return {
    type: 'PAGES',
    payload: pages
  };
};

export const currentPageStore = (page) => {
  return {
    type: 'CURRENT_PAGE',
    payload: page
  };
};

export const categoryStore = (category) => {
  return {
    type: 'CATEGORY_STORE',
    payload: category
  };
};

export const getSupplierMap = ({ startVal, endVal, materialType }) => {
  return (dispatch) => {
    dispatch({ type: 'SUPPLIER_MAP' });
    axios.get('https://pro-api.kcdev.xyz/public/map', {
      params: {
        start: startVal,
        end: endVal,
        material: materialType
      }
    }).then(res => {
      dispatch({ type: 'SUPPLIER_MAP_SUCCESS', payload: res.data });
    }).catch(err => {
      console.log(err);
      dispatch({ type: 'SUPPLIER_MAP_FAIL' });
    });
  };
};

export const getSuppliers = ({ startVal, endVal, materialType }) => {
  return (dispatch) => {
    dispatch({ type: 'SUPPLIERS' });
    axios.get('https://pro-api.kcdev.xyz/public/suppliers', {
      params: {
        start: startVal,
        end: endVal,
        material: materialType
      }
    }).then(res => {
      dispatch({ type: 'SUPPLIERS_SUCCESS', payload: res.data });
    }).catch(err => {
      console.log(err);
      dispatch({ type: 'SUPPLIERS_FAIL' });
    });
  };
};

export const getSupplierPrice = ({ startVal, endVal, materialType }) => {
  return (dispatch) => {
    dispatch({ type: 'SUPPLIER_PRICE' });
    axios.get('https://pro-api.kcdev.xyz/public/price/graph', {
      params: {
        start: startVal,
        end: endVal,
        material: materialType
      }
    }).then(res => {
      dispatch({ type: 'SUPPLIER_PRICE_SUCCESS', payload: res.data });
    }).catch(err => {
      console.log(err);
      dispatch({ type: 'SUPPLIER_PRICE_FAIL' });
    });
  };
};

export const getSupplierList = ({ startVal, endVal, materialType }) => {
  return (dispatch) => {
    dispatch({ type: 'SUPPLIER_LIST' });
    axios.get('https://pro-api.kcdev.xyz/public/suppliers/list', {
      params: {
        start: startVal,
        end: endVal,
        material: materialType
      }
    }).then(res => {
      dispatch({ type: 'SUPPLIER_LIST_SUCCESS', payload: res.data });
    }).catch(err => {
      console.log(err);
      dispatch({ type: 'SUPPLIER_LIST_FAIL' });
    });
  };
};

export const getL1Supplier = ({ materialType }) => {
  return (dispatch) => {
    dispatch({ type: 'SUPPLIER_LIST_L1' });
    axios.get('https://pro-api.kcdev.xyz/public/suppliers/list/L1', {
      params: {
        material: materialType
      }
    }).then(res => {
      dispatch({ type: 'SUPPLIER_LIST_L1_SUCCESS', payload: res.data });
    }).catch(err => {
      console.log(err);
      dispatch({ type: 'SUPPLIER_LIST_L1_FAIL' });
    });
  };
};

export const getL2Supplier = ({ materialType }) => {
  return (dispatch) => {
    dispatch({ type: 'SUPPLIER_LIST_L2' });
    axios.get('https://pro-api.kcdev.xyz/public/suppliers/list/L2', {
      params: {
        material: materialType
      }
    }).then(res => {
      dispatch({ type: 'SUPPLIER_LIST_L2_SUCCESS', payload: res.data });
    }).catch(err => {
      console.log(err);
      dispatch({ type: 'SUPPLIER_LIST_L2_FAIL' });
    });
  };
};

export const getTopOrders = ({ materialType }) => {
  return (dispatch) => {
    dispatch({ type: 'GET_TOP_ORDERS' });
    axios.get('https://pro-api.kcdev.xyz/public/orders/top', {
      params: {
        material: materialType
      }
    }).then(res => {
      dispatch({ type: 'GET_TOP_ORDERS_SUCCESS', payload: res.data });
    }).catch(err => {
      console.log(err);
      dispatch({ type: 'GET_TOP_ORDERS_FAIL' });
    });
  };
};

export const getAllOrder = ({ materialType }) => {
  return (dispatch) => {
    dispatch({ type: 'GET_ALL_ORDERS' });
    axios.get('https://pro-api.kcdev.xyz/public/orders', {
      params: {
        material: materialType
      }
    }).then(res => {
      dispatch({ type: 'GET_ALL_ORDERS_SUCCESS', payload: res.data });
    }).catch(err => {
      console.log(err);
      dispatch({ type: 'GET_ALL_ORDERS_FAIL' });
    });
  };
};

export const getOrderDetails = ({ id, materialType }) => {
  return (dispatch) => {
    dispatch({ type: 'GET_ORDER_DETAILS' });
    axios.get('https://pro-api.kcdev.xyz/public/orders/' + id, {
      params: {
        material: materialType
      }
    }).then(res => {
      dispatch({ type: 'GET_ORDER_DETAILS_SUCCESS', payload: res.data });
    }).catch(err => {
      console.log(err);
      dispatch({ type: 'GET_ORDER_DETAILS_FAIL' });
    });
  };
};

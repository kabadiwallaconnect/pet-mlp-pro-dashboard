import axios from 'axios';

const getVolume = ({ startVal, endVal, materialType }) => {
  return (dispatch) => {
    dispatch({ type: 'VOLUME' });
    axios.get('https://pro-api.kcdev.xyz/public/volume', {
      params: {
        start: startVal,
        end: endVal,
        material: materialType
      }
    }).then(res => {
      console.log(res);
      dispatch({ type: 'VOLUME_SUCCESS', payload: res.data });
    }).catch(err => {
      console.log(err);
      dispatch({ type: 'VOLUME_FAIL' });
    });
  };
};

export default getVolume;

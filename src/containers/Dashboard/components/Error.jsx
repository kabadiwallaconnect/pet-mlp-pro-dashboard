import React from 'react';
import ErrorImage from '../../../shared/img/pluto-page-not-found.svg';

const Error = () => {
  return (
    <div className="dashboard__pageNotFound">
      <img src={ErrorImage} alt="Error404" height={400} />
      <p>Page Not Found</p>
    </div>
  );
};

export default Error;

import React from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import history from '../../App/History';
import { currentPageStore, pageStore } from '../../../redux/actions/supplierActions';

class Pagination extends React.Component {
  componentDidUpdate(prevProps) {
    if (prevProps.currentPage !== this.props.currentPage) {
      history.push("/" + this.props.material + "/L1-Suppliers/page/" + this.props.currentPage);
    }
  }
  
  // handleRightClick() {
  //   console.log('last page Right', this.props.allPages[this.props.allPages.length - 1]);
  //   console.log('pages', this.props.allPages);
  //   console.log('current page Right', this.props.currentPage);
  //   if (this.props.currentPage % 4 === 0 && this.props.currentPage < this.props.allPages[this.props.allPages.length - 1]) {
  //     console.log('Inside if');
  //     this.props.allPages.splice(0, 4);
  //     // const newElement = this.props.allPages[this.props.allPages.length - 1] + 1;
  //     // for (let i=1; i <= 4; i++) {
  //     //   const element = this.props.allPages[this.props.allPages.length - 1] + i;
  //     //   this.props.allPages.push(element);
  //     // }
  //     // this.props.allPages.push(newElement);
  //     console.log(this.props.allPages);
  //     this.props.pageStore(this.props.allPages);
  //   }
  //   if (this.props.currentPage < this.props.allPages[this.props.allPages.length - 1]) {
  //     this.props.currentPageStore(this.props.currentPage + 1);
  //   }
  //   // if (this.props.currentPage + 4 < this.props.allPages[this.props.allPages.length - 1]) {
  //   //   const newElement = this.props.allPages[this.props.allPages.length - 1] + 1;
  //   //   this.props.allPages.push(newElement);
  //   // }
  // }
  // 
  // 
  // handleLeftClick() {
  //   console.log('last page Left', this.props.allPages[this.props.allPages.length - 1]);
  //   console.log('current page Left', this.props.currentPage);
  //   if (this.props.allPages[this.props.allPages.length - 1] >= this.props.currentPage + 3 && this.props.currentPage[0] !== 1) {
  //     this.props.allPages.pop();
  //     const newElement = this.props.allPages[0] - 1;
  //     this.props.allPages.unshift(newElement);
  //     this.props.pageStore(this.props.allPages);
  //   }
  //   if (this.props.currentPage > 1) {
  //     this.props.currentPageStore(this.props.currentPage - 1);
  //   }
  // }
  
  handleRightClick() {
    const { currentPage } = this.props;
    if (this.props.currentPage < this.props.allPages[this.props.allPages.length - 1]) {
      const newCurrentPage = currentPage + 1;
      this.props.currentPageStore(newCurrentPage);
      if (this.props.allPages.indexOf(newCurrentPage) > 3 && this.props.allPages.indexOf(newCurrentPage) + 1 <= this.props.allPages.length) {
        console.log('Inside If');
        console.log(this.props.allPages);
        this.props.allPages.shift();
        console.log(this.props.allPages);
        // const newElement = this.props.allPages[2] + 1;
        // this.props.allPages.push(newElement);
        // console.log(this.props.allPages);
        this.props.pageStore(this.props.allPages);
      }  
    }
  }
  
  handleLeftClick() {
    const { currentPage } = this.props;
    if (currentPage > 1) {
      const newCurrentPage = currentPage - 1;
      this.props.currentPageStore(newCurrentPage);
      if (this.props.allPages.indexOf(currentPage) === 0) {
        const newElement = this.props.allPages[0] - 1;
        this.props.allPages.unshift(newElement);
        console.log(this.props.allPages);
        this.props.pageStore(this.props.allPages);
      }
    }
  }
  
  hadlePageClick(page) {
    this.props.currentPageStore(page);
  }
  
  render() {
    const { currentPage } = this.props;
    return (
      <div className="pagination__container">
        <ul>
          <button className="pagination__button" onClick={() => this.handleLeftClick()}>&#x2190;</button>
          {
            this.props.allPages.map((page, index) => (
              index <= 3 ? (
                <button 
                  onClick={() => this.hadlePageClick(page)}
                  className={page === currentPage ? "selectedPageStyle" : "notSelectedPageStyle"}
                >
                  {page}
                </button>
              ) : null  
            ))
          }
          <button className="pagination__button" onClick={() => this.handleRightClick()}>&#x2192;</button>
        </ul>
      </div>
      
    );
  }
}

Pagination.propTypes = {
  allPages: propTypes.array.isRequired,
  material: propTypes.string.isRequired,
  currentPageStore: propTypes.func.isRequired,
  currentPage: propTypes.number.isRequired,
  pageStore: propTypes.number.isRequired
};

const mapStateToProps = state => {
  return {
    currentPage: state.supplier.currentPage,
    allPages: state.supplier.allPages
  };
};

export default connect(
  mapStateToProps,
  {
    currentPageStore,
    pageStore
  }
)(Pagination);

import React from 'react';
import { Card, CardBody, Col } from 'reactstrap';
import propTypes from 'prop-types';

const Revenue = (props) => {
  return (
    <Col md={12} xl={4} lg={6} xs={12}>
      <Card>
        <CardBody className="dashboard__card-widget">
          <div className="card__title">
            <h5>RECYCLED VOLUME</h5>
          </div>
          <div className="dashboard__total dashboard__total--area">
            <p className="dashboard__total-stat">
              {props.value}
              <span className="unit"> TONS</span>
              {
                props.material === "PET" ? (
                  <span className="unit__desc unit__desc--inline"> OF PET FLAKES</span>
                ) : (
                  <span className="unit__desc unit__desc--inline"> OF FLEXIBLES</span>
                )
              }
            </p>
          </div>
        </CardBody>
      </Card>
    </Col>
  );
};

Revenue.propTypes = {
  value: propTypes.number,
  material: propTypes.string
};

Revenue.defaultProps = {
  value: 0,
  material: ''
};

export default Revenue;

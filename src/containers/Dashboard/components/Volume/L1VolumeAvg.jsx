import React from 'react';
import { Card, CardBody, Col } from 'reactstrap';
import propTypes from 'prop-types';

const Volume = (props) => {
  return (
    <Col md={12} xl={3} lg={6} xs={12}>
      <Card>
        <CardBody className="dashboard__card-widget">
          <div className="card__title">
            <h5>AVERAGE VOLUME PER L1</h5>
          </div>
          <div className="dashboard__total dashboard__total--area">
            <p className="dashboard__total-stat">
              {props.value}
              <span className="unit"> TONS</span>
            </p>
          </div>
        </CardBody>
      </Card>
    </Col>
  );
};

Volume.propTypes = {
  value: propTypes.number
};

Volume.defaultProps = {
  value: 0
};

export default Volume;

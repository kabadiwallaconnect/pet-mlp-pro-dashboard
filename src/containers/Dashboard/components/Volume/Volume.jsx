import React from 'react';
import { Card, CardBody, Col } from 'reactstrap';
import propTypes from 'prop-types';

const Volume = (props) => {
  return (
    <Col md={12} xl={4} lg={6} xs={12}>
      <Card>
        <CardBody className="dashboard__card-widget">
          <div className="card__title">
            <h5>RECOVERED VOLUME</h5>
          </div>
          <div className="dashboard__total dashboard__total--area">
            <p className="dashboard__total-stat">
              {props.value.total}
              <span className="unit"> TONS</span>
            </p>
          </div>
        </CardBody>
      </Card>
    </Col>
  );
};

Volume.propTypes = {
  value: propTypes.object
};

Volume.defaultProps = {
  value: {}
};

export default Volume;

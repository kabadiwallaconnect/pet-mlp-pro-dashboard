import React from 'react';
import { Card, CardBody, Col } from 'reactstrap';
import propTypes from 'prop-types';

const L1Volume = (props) => {
  return (
    <Col md={12} xl={3} lg={6} xs={12}>
      <Card>
        <CardBody className="dashboard__card-widget-large">
          <div className="card__title">
            <h5>TOTAL VOLUME FROM L1</h5>
          </div>
          <div className="dashboard__total dashboard__total--area">
            <p className="dashboard__total-stat">
              {props.value.l1_volume }
              <span className="unit"> TONS</span>
              <span className="unit__desc unit__desc--block">SOURCED FROM {props.value.l1_count} L1 Suppliers</span>
            </p>
          </div>
        </CardBody>
      </Card>
    </Col>
  );
};


L1Volume.propTypes = {
  value: propTypes.object
};

L1Volume.defaultProps = {
  value: {}
};

export default L1Volume;

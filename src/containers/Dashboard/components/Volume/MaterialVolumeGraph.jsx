/* eslint-disable no-underscore-dangle,react/no-did-mount-set-state */
import React from 'react';
import { Card, CardBody, Col } from 'reactstrap';
import { Line } from 'react-chartjs-2';
// import { translate } from 'react-i18next';
import propTypes from 'prop-types';

const options = {
  legend: {
    position: 'bottom',
    labels: {
      fontSize: 12,
      fontColor: '#818ea3',
      fontFamily: "'Open Sans', sans-serif",
      // fontWeight: 600
    },
  },
  tooltips: {
    enabled: true,
    bodyFontSize: 11,
    bodyFontColor: "white",
    bodyFontFamily: "'Open Sans', sans-serif",
    bodyFontStyle: "400",
    titleFontSize: 12,
    xPadding: 10,
    yPadding: 10,
    backgroundColor: '#374965',
    titleFontFamily: "'Open Sans', sans-serif",
    titleFontStyle: "600",
    cornerRadius: 3,
    displayColors: false,
    callbacks: {
      label: (tooltipItems) => { 
        return 'Volume: ' + tooltipItems.yLabel + ' Kg';
      }
    }
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          color: '#fff',
          borderDash: [1, 1],
        },
        ticks: {
          fontColor: '#66748b',
          fontFamily: "'Open Sans', sans-serif",
          fontSize: 0
        },
      },
    ],
    yAxes: [
      {
        gridLines: {
          color: 'rgb(204, 204, 204)',
          borderDash: [1, 3],
        },
        scaleLabel: {
          display: false,
          labelString: 'Volume (TONS)',
          fontSize: 12,
          fontColor: '#818ea3',
          fontFamily: "'Open Sans', sans-serif"
        },
        ticks: {
          fontColor: '#66748b',
          fontSize: 10,
        },
      },
    ],
  },
};

const renderLabelName = (labels) => {
  const materialName = [];
  for (let i = 0; i < labels.length; i++) {
    const index = labels[i].indexOf("(");
    const material = labels[i].slice(0, index);
    materialName.push(material);
  }
  return materialName;
};

const MaterialVolumeGraph = (props) => {
  // const { t } = this.props;
  const mapData = {
    labels: renderLabelName(props.labels),
    datasets: [
      {
        label: 'Volume (Kg)',
        backgroundColor: '#dbfbec',
        borderColor: '#16C172',
        // lineTension: 0.1,
        borderJointStyle: 'miter',
        borderWidth: 1,
        pointHoverBorderWidth: 2,
        borderCapStyle: 'butt',
        pointHoverBackgroundColor: '#18d07b',
        pointHitRadius: 7,
        data: props.axis,
      },
    ],
  };
  
  return (
    <Col md={12} lg={12} xl={12}>
      <Card>
        <CardBody>
          <div className="card__title card__titleMargin">
            <h5>VOLUME RECOVERED</h5>
          </div>
          <Line height={80} data={mapData} options={options} />
        </CardBody>
      </Card>
    </Col>
  );
};

MaterialVolumeGraph.propTypes = {
  labels: propTypes.array,
  axis: propTypes.array
};

MaterialVolumeGraph.defaultProps = {
  labels: [],
  axis: []
};

export default MaterialVolumeGraph;

// export default translate('common')(RandomAnimatedBars);

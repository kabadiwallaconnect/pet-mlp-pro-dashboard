import React from 'react';
import { Col } from 'reactstrap';
import "react-responsive-carousel/lib/styles/carousel.css";
import { Carousel } from 'react-responsive-carousel';
import PlaceOrder from '../../../../shared/img/PlaceOrder.jpg';
import Unloading from '../../../../shared/img/Unloading.jpg';
import Collection from '../../../../shared/img/Collection.jpg';
import MRFProcessing from '../../../../shared/img/MRF.jpg';
import Flakes from '../../../../shared/img/Flakes.jpg';


const ImageCarousel = () => {
  return (
    <Col md={12} xl={12} lg={12} xs={12}>
      <Carousel infiniteLoop height={300} >
        <div style={{ marginRight: 0 }}>
          <img src={PlaceOrder} alt="" />
          <p className="legend">Legend 1</p>
        </div>
        <div style={{ marginRight: 0 }}>
          <img src={Unloading} alt="" />
          <p className="legend">Legend 2</p>
        </div>
        <div style={{ marginRight: 0 }}>
          <img src={Collection} alt="" />
          <p className="legend">Legend 3</p>
        </div>
        <div style={{ marginRight: 0 }}>
          <img src={MRFProcessing} alt="" />
          <p className="legend">Legend 4</p>
        </div>
        <div style={{ marginRight: 0 }}>
          <img src={Flakes} alt="" />
          <p className="legend">Legend 5</p>
        </div>
      </Carousel>
    </Col>
  );
};
 
export default ImageCarousel;

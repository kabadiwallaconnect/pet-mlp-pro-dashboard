import React from 'react';
import { Card, CardBody, Col } from 'reactstrap';
import propTypes from 'prop-types';

const L1Onboard = (props) => {
  return (
    <Col md={12} xl={3} lg={6} xs={12}>
      <Card>
        <CardBody className="dashboard__card-widget">
          <div className="card__title">
            <h5>TRANSPORTATION COST</h5>
          </div>
          <div className="dashboard__total dashboard__total--area">
            <p className="dashboard__total-stat">
              {props.value.toString().replace(/(\d)(?=(\d\d)+\d$)/g, "$1,")}
              <span className="unit"> INR</span>
            </p>
          </div>
        </CardBody>
      </Card>
    </Col>
  );
};

L1Onboard.propTypes = {
  value: propTypes.number
};

L1Onboard.defaultProps = {
  value: 0
};

export default L1Onboard;

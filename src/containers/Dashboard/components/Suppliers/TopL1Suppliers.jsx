import React from 'react';
import { Card, CardBody, Table } from 'reactstrap';
// import { translate } from 'react-i18next';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import { Link } from 'react-router-dom';

const TopL1Suppliers = (props) => (
  <Card>
    <CardBody>
      <div className="card__title card__titleMargin card__title--row">
        <h5>TOP 5 L1 Suppliers</h5>
        <div><Link className="button" to={{ pathname: "/" + props.material + "/L1" }}>View All</Link></div>
      </div>
      <Table className="table--bordered table--head-accent" responsive striped>
        <thead>
          <tr>
            <th>User ID</th>
            <th>Name</th>
            <th>Volume (Tons)</th>
            <th>Quality</th>
          </tr>
        </thead>
        <tbody>
          {
            props.suppliers.map((supplier) => {
              return (
                <tr key={Number(supplier.user_id)}>
                  <td><Link to={{ pathname: props.material + "/profile/" + supplier.user_id, state: { message: props.material } }}>{supplier.user_id}</Link></td>
                  <td>
                    {
                      supplier.name
                    } 
                    {
                      supplier.id_proof_type !== '-' ? (
                        <span style={{ color: 'green' }}> &#10004;</span>
                      ) : null
                    }
                  </td>
                  <td>{parseFloat(supplier.quantity).toFixed(2)}</td>
                  <td>
                    {Math.round(supplier.quality)}%
                  </td>
                </tr>
              ); 
            })
          }
        </tbody>
      </Table>
    </CardBody>
  </Card>  
);

TopL1Suppliers.propTypes = {
  suppliers: propTypes.array,
  material: propTypes.string.isRequired
};

TopL1Suppliers.defaultProps = {
  suppliers: []
};

const mapStateToProps = () => {
  return {
    
  };
};

export default connect(
  mapStateToProps, 
  {
    
  }
)(TopL1Suppliers);
// export default translate('common')(HeadAccentTable);

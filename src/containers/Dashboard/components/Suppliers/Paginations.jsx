import React, { Component } from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import history from '../../../App/History';
import { currentPageStore } from '../../../../redux/actions/supplierActions';

class Pagination extends Component {
  componentDidUpdate(prevProp) {
    console.log('this.props', this.props);
    console.log('prevProp', prevProp);
    if (this.props.currentPage !== prevProp.currentPage) {
      if (this.props.category === "orders") {
        history.replace("/" + this.props.material + "/orders/page/" + this.props.currentPage);
      } else if (this.props.category === "L1") {
        history.replace("/" + this.props.material + "/L1-Suppliers/page/" + this.props.currentPage);
      } else {
        history.replace("/" + this.props.material + "/L2-Suppliers/page/" + this.props.currentPage);
      }
    }
  }
  
  handleLeftClick() {
    const { currentPage } = this.props;
    if (currentPage > 1) {
      const newCurrentPage = currentPage - 1;
      console.log('newCurrentPage', newCurrentPage);
      this.props.currentPageStore(newCurrentPage);
    }
  }
  
  handleRightClick() {
    const { currentPage } = this.props;
    if ((currentPage * 50) < 1000) {
      const newCurrentPage = currentPage + 1;
      console.log('newCurrentPage', newCurrentPage);
      this.props.currentPageStore(newCurrentPage);
    }
  }
  
  render() {
    const { currentPage } = this.props;
    return (
      <div className="pagination__container">
        <div className="pagination__pages">{((currentPage-1) * 50)+1} - {currentPage*50} of 1000</div>
        <button className="pagination__button-white" onClick={() => this.handleLeftClick()}>&#x2190;</button>
        <button className="pagination__button-white u-margin-right-40" onClick={() => this.handleRightClick()}>&#x2192;</button>
      </div>
    );
  }
}

Pagination.propTypes = {
  currentPageStore: propTypes.func.isRequired,
  currentPage: propTypes.number.isRequired,
  material: propTypes.string.isRequired,
  category: propTypes.string.isRequired
};

const mapStateToProps = (state) => {
  return {
    currentPage: state.supplier.currentPage
  };
};

export default connect(
  mapStateToProps,
  {
    currentPageStore
  }
)(Pagination); 

import React, { Component } from 'react';
import propTypes from 'prop-types';
import { filter } from 'fuzzaldrin-plus';
import {
  Table,
  Popover,
  Position,
  Menu,
  Avatar,
  Text,
  TextDropdownButton
} from 'evergreen-ui';
import { connect } from 'react-redux';
import Loader from '../../components/Loader';
// import Pagination from './Paginations';
import Topbar from '../../../Layout';
import history from '../../../App/History';
import { 
  currentPageStore, 
  getL2Supplier,
  categoryStore 
} from '../../../../redux/actions/supplierActions';

const Order = {
  NONE: 'NONE',
  ASC: 'ASC',
  DESC: 'DESC'
};

class KCL2Suppliers extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      searchQuery: '', // Fuzzy Search query state
      orderedColumn: 6,
      ordering: Order.DESC,
      column2Show: 'User ID' 
    };
  }
  
  componentDidMount() {
    const category = "L2";
    const materialType = this.props.match.params.material; 
    const page = Number(this.props.match.params.page);
    this.props.currentPageStore(page);
    this.props.categoryStore(category);
    this.props.getL2Supplier({ materialType });
  }
  
  componentDidUpdate(prevProp) {
    if (this.props.match.params.material !== prevProp.match.params.material) {
      this.getDetails();
    }
  }
  
  onSelectRow(profile) {
    const materialType = this.props.match.params.material;
    history.push('/' + materialType + "/profile/" + profile.user_id);
  }
  
  getDetails() {
    const materialType = this.props.match.params.material; 
    this.props.getL2Supplier({ materialType });
  }
  
  getIconForOrder = order => {
    switch (order) {
      case Order.ASC:
        return 'arrow-up';
      case Order.DESC:
        return 'arrow-down';
      default:
        return 'caret-down';
    }
  }
  
  sort = (L1Suppliers) => {
    // Ordering - Type of ordering ('ASC' or 'DESC')
    // orderedColumn - Column number based on which table is sorted
    const { ordering, orderedColumn } = this.state;
    // Return if there's no ordering.
    if (ordering === Order.NONE) return L1Suppliers;

    // Get the property to sort each profile on.
    // By default use the `name` property.
    let propKey = 'name';
    
    if (orderedColumn === 4) propKey = 'quantity';
    if (orderedColumn === 5) propKey = 'quality';
    if (orderedColumn === 6) propKey = 'orders';
    return L1Suppliers.sort((a, b) => {
      let aValue = a[propKey];
      let bValue = b[propKey];
      aValue = Number(aValue);
      bValue = Number(bValue);

      // Support string comparison
      // Sorting conditions (-1 -> a comes first, 1 -> b comes first, 0 -> a equals b)
      const sortTable = { true: 1, false: -1 };
      
      // Order ascending (Order.ASC)
      if (this.state.ordering === Order.ASC) {
        return aValue === bValue ? 0 : sortTable[aValue > bValue];
      } else {
        // Order descending (Order.DESC)
        return bValue === aValue ? 0 : sortTable[bValue > aValue];
      }  
    });
  }
  
  // Filter the profiles based on the name property.
  filter = (supplierList) => {
    // const { supplierList } = this.props;
    const searchQuery = this.state.searchQuery.trim();
    // If the searchQuery is empty, return the profiles as is.
    if (searchQuery.length === 0) return supplierList;
    
    return supplierList.filter(profile => {
      const result = filter([profile.name, profile.user_id, profile.quantity, profile.quality], searchQuery);
      return result.length === 1;
    });
  }
  
  handleFilterChange = value => {
    this.setState({ searchQuery: value });
  }
  
  handleFilterChangeValue(evt) {
    this.setState({ searchQuery: evt.target.value });
  }
  
  renderNameTableHeaderCell = () => {
    return (
      <Table.HeaderCell>
        <div className="headerCell left-spacing">Name</div>
      </Table.HeaderCell>
    );
  }
  
  // User ID
  renderValueTableHeaderCell = () => {
    return (
      <Table.HeaderCell>
        <div className="headerCell">User ID</div>
      </Table.HeaderCell>
    );
  }

  // Volume
  renderVolumeTableHeaderCell = () => {
    return (
      <Table.TextHeaderCell>
        <Popover
          position={Position.BOTTOM_LEFT}
          content={({ close }) => (
            <Menu>
              <Menu.OptionsGroup
                title="Order"
                options={[
                  { label: 'Ascending', value: Order.ASC },
                  { label: 'Descending', value: Order.DESC }
                ]}
                selected={
                  this.state.orderedColumn === 4 ? this.state.ordering : null
                }
                onChange={value => {
                  console.log('filter on onChange', value);
                  this.setState({
                    orderedColumn: 4,
                    ordering: value
                  });
                  close();
                }}
              />
            </Menu>
          )}
        >
          <TextDropdownButton
            icon={
              this.state.orderedColumn === 4
                ? this.getIconForOrder(this.state.ordering)
                : 'caret-down'
            }
          >
            <div className="headerCell">Volume (Tons)</div>
          </TextDropdownButton>
        </Popover>
      </Table.TextHeaderCell>
    );
  }
  
  // Average Quality
  renderQualityTableHeaderCell = () => {
    return (
      <Table.TextHeaderCell>
        <Popover
          position={Position.BOTTOM_LEFT}
          content={({ close }) => (
            <Menu>
              <Menu.OptionsGroup
                title="Order"
                options={[
                  { label: 'Ascending', value: Order.ASC },
                  { label: 'Descending', value: Order.DESC }
                ]}
                selected={
                  this.state.orderedColumn === 5 ? this.state.ordering : null
                }
                onChange={value => {
                  this.setState({
                    orderedColumn: 5,
                    ordering: value
                  });
                  close();
                }}
              />
            </Menu>
          )}
        >
          <TextDropdownButton
            icon={
              this.state.orderedColumn === 5
                ? this.getIconForOrder(this.state.ordering)
                : 'caret-down'
            }
          >
            <div className="headerCell">Quality</div>
          </TextDropdownButton>
        </Popover>
      </Table.TextHeaderCell>
    );
  }
  
  // Number Of Orders
  renderOrdersTableHeaderCell = () => {
    return (
      <Table.TextHeaderCell>
        <Popover
          position={Position.BOTTOM_LEFT}
          content={({ close }) => (
            <Menu>
              <Menu.OptionsGroup
                title="Order"
                options={[
                  { label: 'Ascending', value: Order.ASC },
                  { label: 'Descending', value: Order.DESC }
                ]}
                selected={
                  this.state.orderedColumn === 6 ? this.state.ordering : null
                }
                onChange={value => {
                  this.setState({
                    orderedColumn: 6,
                    ordering: value
                  });
                  close();
                }}
              />
            </Menu>
          )}
        >
          <TextDropdownButton
            icon={
              this.state.orderedColumn === 6
                ? this.getIconForOrder(this.state.ordering)
                : 'caret-down'
            }
          >
            <div className="headerCell">No. of Orders</div>
          </TextDropdownButton>
        </Popover>
      </Table.TextHeaderCell>
    );
  }
  
  // Mobile Number
  renderMobileTableHeaderCell = () => {
    return (
      <Table.TextHeaderCell>
        Mobile Number
      </Table.TextHeaderCell>
    );
  }
  
  // Table Row Values
  renderRow = ({ profile }) => {
    return (
      <Table.Row key={profile.user_id} isSelectable onSelect={() => this.onSelectRow(profile)}>
        <Table.Cell flexBasis={560} flexShrink={0} flexGrow={0} display="flex" alignItems="center">
          <Avatar name={profile.name} />
          <Text marginLeft={8} size={300} fontWeight={500}>
            {profile.name}
          </Text>
        </Table.Cell>
        <Table.TextCell flexBasis={560} flexShrink={0} flexGrow={0} className="table__font"><Text fontFamily={"'Open Sans', sans-serif"} fontSize={12}>{profile.user_id}</Text></Table.TextCell>
        <Table.TextCell isNumber flexBasis={560} flexShrink={0} flexGrow={0} className="table__font"><Text fontFamily={"'Open Sans', sans-serif"} fontSize={12}>{profile.quantity ? parseFloat(profile.quantity).toFixed(2) : null}</Text></Table.TextCell>
        <Table.TextCell isNumber flexBasis={560} flexShrink={0} flexGrow={0} className="table__font"><Text fontFamily={"'Open Sans', sans-serif"} fontSize={12}>{profile.quality === 0 ? "-" : (profile.quality + "%")}</Text></Table.TextCell>
        <Table.TextCell isNumber flexBasis={560} flexShrink={0} flexGrow={0} className="table__font"><Text fontFamily={"'Open Sans', sans-serif"} fontSize={12}>{profile.orders}</Text></Table.TextCell>
      </Table.Row>
    );
  }


  render() {
    const { supplierListL2Loading, supplierList } = this.props;
    const items = this.filter(this.sort(supplierList));
    return (
      <div>
        <Topbar />
        {
          supplierListL2Loading ? (
            <Loader load={supplierListL2Loading} />
          ) : (
            <div className="table__container__wrap table__container">
              {/* <div className="searchTable__search-container">
                <div className="searchTable__search-text">Search</div>
                <input 
                  className="searchTable__search-input"
                  onChange={(evt) => this.handleFilterChangeValue(evt)}
                  value={this.state.searchQuery}
                />
              </div> */}
              <Table border>
                <Table.Head intent="success">
                  {this.renderNameTableHeaderCell()}
                  {this.renderValueTableHeaderCell()}
                  {this.renderVolumeTableHeaderCell()}
                  {this.renderQualityTableHeaderCell()}
                  {this.renderOrdersTableHeaderCell()}
                </Table.Head>
                <Table.VirtualBody height={window.innerHeight - 150}>
                  {items.map((item, index) => this.renderRow({ profile: item, index }))}
                </Table.VirtualBody>
              </Table>
            </div>
          )
        }
        {/* <Pagination 
          material={this.props.match.params.material}
          category={this.props.category}
        /> */}
      </div>
    );
  }
}

KCL2Suppliers.propTypes = {
  match: propTypes.object,
  currentPageStore: propTypes.func.isRequired,
  categoryStore: propTypes.func.isRequired,
  // category: propTypes.string.isRequired,
  getL2Supplier: propTypes.func.isRequired,
  supplierListL2Loading: propTypes.bool.isRequired,
  supplierList: propTypes.array.isRequired,
};

KCL2Suppliers.defaultProps = {
  match: {}
};

const mapStateToProps = (state) => {
  return {
    supplierListL2Loading: state.supplier.supplierListL2Loading,
    supplierList: state.supplier.supplierListL2,
    category: state.supplier.category
  };
};

export default connect(
  mapStateToProps,
  {
    currentPageStore,
    getL2Supplier,
    categoryStore
  }
)(KCL2Suppliers);

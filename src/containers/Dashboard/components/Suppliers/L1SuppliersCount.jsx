import React from 'react';
import { Card, CardBody, Col } from 'reactstrap';
import propTypes from 'prop-types';

const L1Suppliers = (props) => {
  return (
    <Col md={12} xl={4} lg={6} xs={12}>
      <Card>
        <CardBody className="dashboard__card-widget">
          <div className="card__title">
            <h5>NUMBER OF L1 SUPPLIERS</h5>
          </div>
          <div className="dashboard__total dashboard__total--area">
            <p className="dashboard__total-stat">
              {props.value}
            </p>
          </div>
        </CardBody>
      </Card>
    </Col>
  );
};

L1Suppliers.propTypes = {
  value: propTypes.number
};

L1Suppliers.defaultProps = {
  value: 0
};

export default L1Suppliers;

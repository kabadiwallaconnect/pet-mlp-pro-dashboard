/* eslint-disable no-underscore-dangle,react/no-did-mount-set-state */
import React from 'react';
import { Card, CardBody, Col } from 'reactstrap';
import { Line } from 'react-chartjs-2';
// import { translate } from 'react-i18next';
import propTypes from 'prop-types';
import Loader from '../../components/Loader';

const options = {
  responsive: true,
  hover: {
    mode: 'point'
  },
  animation: {
    duration: 0,
    easing: 'linear'
  },
  legend: {
    position: 'bottom',
    labels: {
      fontSize: 11,
      fontColor: '#818ea3',
      fontFamily: "'Open Sans', sans-serif",
      // fontWeight: 600
    },
  },
  tooltips: {
    enabled: true,
    bodyFontSize: 11,
    bodyFontColor: "white",
    bodyFontFamily: "'Open Sans', sans-serif",
    bodyFontStyle: "400",
    titleFontSize: 12,
    xPadding: 10,
    yPadding: 10,
    backgroundColor: '#374965',
    titleFontFamily: "'Open Sans', sans-serif",
    titleFontStyle: "600",
    cornerRadius: 6,
    displayColors: false,
    callbacks: {
      label: (tooltipItems) => { 
        if (tooltipItems.datasetIndex === 0) {
          return 'L1 Price: ' + tooltipItems.yLabel + ' INR';
        } else if (tooltipItems.datasetIndex === 1) {
          return 'L2 Price: ' + tooltipItems.yLabel + ' INR';
        }
      }
    }
  },
  scales: {
    xAxes: [
      {
        stacked: false,
        gridLines: {
          color: '#fff',
          borderDash: [1, 1],
        },
        ticks: {
          fontColor: '#66748b',
          fontFamily: "'Open Sans', sans-serif",
          fontSize: 10
        },
      },
    ],
    yAxes: [
      {
        stacked: false,
        gridLines: {
          color: 'rgb(204, 204, 204)',
          borderDash: [1, 3],
        },
        scaleLabel: {
          display: false,
          labelString: 'Volume (Kg)',
          fontSize: 12,
          fontColor: '#818ea3',
          fontFamily: "'Open Sans', sans-serif"
        },
        ticks: {
          fontColor: '#66748b',
          fontSize: 10,
        },
      },
    ],
  },
};

const Price = (props) => {
  // const { t } = this.props;
  const mapData = {
    labels: props.labels,
    datasets: [
      {
        label: 'L1 Price',
        data: props.L1_price,
        pointBackgroundColor: '#e0e5ee',
        pointBorderColor: '#4f6991',
        pointHoverBackgroundColor: '#4f6991',
        pointHoverBorderWidth: 1,
        backgroundColor: '#e0e5ee',
        borderColor: '#4f6991',
        borderWidth: 1,
        pointHitRadius: 5,
        pointRadius: 0.5,
        // lineTension: 0
      },
      {
        label: 'L2 Price',
        data: props.L2_price,
        pointBackgroundColor: '#adf6d4',
        pointBorderColor: '#16C172',
        pointHoverBackgroundColor: '#16C172',
        pointHoverBorderWidth: 1,
        backgroundColor: '#adf6d4',
        borderColor: '#16C172',
        borderWidth: 1,
        pointHitRadius: 5,
        pointRadius: 0.5,
        // lineTension: 0
      },
      // {
      //   label: 'Market Buying Price for L2',
      //   data: props.market_price_L2,
      //   pointBackgroundColor: '#b4c0d6',
      //   pointBorderColor: '#4f6991',
      //   pointHoverBackgroundColor: '#4f6991',
      //   pointHoverBorderWidth: 2,
      //   backgroundColor: '#b4c0d6',
      //   borderColor: '#4f6991',
      //   borderWidth: 1,
      //   pointHitRadius: 10,
      // },
      // {
      //   label: 'KC Buying Price for L2',
      //   data: props.kc_price_L2,
      //   pointBackgroundColor: '#adf6d4',
      //   pointBorderColor: '#16C172',
      //   pointHoverBackgroundColor: '#16C172',
      //   pointHoverBorderWidth: 2,
      //   backgroundColor: '#adf6d4',
      //   borderColor: '#16C172',
      //   borderWidth: 1,
      //   pointHitRadius: 2,
      // },
    ]
  };
  
  return (
    <Col md={12} lg={12} xl={12}>
      {
        props.labels.length === 0 ? (
          <div className="card__centerAlign">
            <Loader load />
          </div>
        ) : (
          <Card>
            <CardBody>
              <div className="card__title card__titleMargin">
                <h5>
                  KC PRICE FOR L1 and L2
                  {/* <span className="card__sub"><a target="_blank" rel="noopener noreferrer" href="https://drive.google.com/open?id=1miZqmqULnM0GzZG0vqdivfHBP2HlsKUv">PURCHASE REGISTER</a></span> */}
                  {/* <span className="card__sub"><a href="https://drive.google.com/drive/u/1/folders/1h3h_xnCh7jCjHSWRP58j2rECfpkNSU3_">SELLING INVOICE</a></span> */}
                </h5>
              </div>
              <Line height={170} data={mapData} options={options} />
            </CardBody>
          </Card>
        )
      }
    </Col>
  );
};

Price.propTypes = {
  labels: propTypes.array,
  L1_price: propTypes.array,
  // market_price_L2: propTypes.array,
  L2_price: propTypes.array,
  // kc_price_L2: propTypes.array
};

Price.defaultProps = {
  labels: [],
  L1_price: [],
  // market_price_L2: [],
  L2_price: [],
  // kc_price_L2: []
};

export default Price;

// export default translate('common')(RandomAnimatedBars);

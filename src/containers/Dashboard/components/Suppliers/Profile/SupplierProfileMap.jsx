/* eslint-disable jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */
import React from 'react';
import { Col } from 'reactstrap';
import { compose, withProps, withStateHandlers } from 'recompose';
import { GoogleMap, Marker, withGoogleMap, withScriptjs } from 'react-google-maps';
import { InfoBox } from 'react-google-maps/lib/components/addons/InfoBox';
// import { translate } from 'react-i18next';
import propTypes from 'prop-types';
import greenMarker from '../../../../../shared/img/greenMarker.svg';
import mapStyle from '../../../../../shared/googleMapStyle.json';

const MapWithAMarker = compose(
  withProps({
    // generate your API key
    googleMapURL: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBSDP8y2ydNDx6Fcloubk7bWZfx1c4s_6U',
    loadingElement: <div style={{ height: '100%' }} />,
    containerElement: <div className="map" style={{ height: '40vh' }} />,
    mapElement: <div style={{ height: '100%' }} />,
  }),
  withStateHandlers(() => ({
    isOpen: true,
    markerIndex: 0
  }), {
    onToggleOpen: () => (index) => ({
      isOpen: true,
      markerIndex: index
    }),
  }),
  withScriptjs,
  withGoogleMap,
)(props => (
  <GoogleMap
    defaultOptions={{ 
      scrollwheel: false, 
      streetViewControl: false, 
      mapTypeControl: false, 
      styles: mapStyle 
    }}
    defaultZoom={10}
    defaultCenter={{ lat: Number(props.latitude), lng: Number(props.longitude) }}
  >
    {props.isMarkerShown &&
      <Marker 
        icon={greenMarker}
        position={{ lat: Number(props.latitude), lng: Number(props.longitude) }} 
        // onClick={() => this.setState({ showInfoIndex: index })}
        // onClick={props.onMarkerClick(index)}
      >
        {
          props.isOpen &&
          <InfoBox 
            defaultPosition={new google.maps.LatLng(props.latitude, props.longitude)}
            options={{ 
              closeBoxURL: '', 
              enableEventPropagation: true, 
              alignBottom: true
            }}
          >
            <div className="map__marker-label">
              <div className="map__marker-label-content">
                {
                  props.name
                }  
              </div>
            </div>
          </InfoBox>
        }
      </Marker>
    }
  </GoogleMap>
));

class BasicMap extends React.PureComponent {
  render() {
    console.log(this.props);
    return (
      <Col xs={12} md={12} lg={12}>
        <div className="map__map-container">
          <MapWithAMarker 
            isMarkerShown 
            latitude={this.props.supplierLatitude} 
            longitude={this.props.supplierLongitude}
            name={this.props.name}
          />
        </div>
      </Col>
    );
  }
}

BasicMap.propTypes = {
  supplierLatitude: propTypes.string,
  supplierLongitude: propTypes.string,
  name: propTypes.string.isRequired
};

BasicMap.defaultProps = {
  supplierLatitude: [],
  supplierLongitude: []
};

// export default translate('common')(BasicMap);
export default BasicMap;

import React from 'react';
import { Card, CardBody, Col, Row } from 'reactstrap';
import axios from 'axios';
import propTypes from 'prop-types';
import SupplierProfileMap from './SupplierProfileMap';
import OrderTable from './OrdersTable';
import Topbar from '../../../../Layout';

export default class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = { supplierProfile: '' };
  }
  componentDidMount() {
    const id = this.props.match.params.id;
    const materialType = this.props.match.params.material;
    // Suppliers Profile
    axios.get('https://pro-api.kcdev.xyz/public/suppliers/' + id, {
      params: {
        start: '2016-04-13',  
        end: '2019-01-01',
        material: materialType
      }
    }).then(res => {
      this.setState({ supplierProfile: res.data, loading: false });
    });
  }
  
  componentDidUpdate(prevProps) {
    if (this.props.match.params.id !== prevProps.match.params.id || this.props.match.params.material !== prevProps.match.params.material) {
      this.getSupplierProfile(this.props.match.params.id, this.props.match.params.material);
    }
  }
  
  getSupplierProfile(id, materialType) {
    axios.get('https://pro-api.kcdev.xyz/public/suppliers/' + id, {
      params: {
        start: '2016-04-13',  
        end: '2019-01-01',
        material: materialType
      }
    }).then(res => {
      console.log(res, 'profile');
      this.setState({ supplierProfile: res.data, loading: false });
    });
  }
  
  renderQuantity() {
    if (this.props.match.params.material === "PET") {
      return parseFloat(this.state.supplierProfile.materials.PET.quantity/1000).toFixed(2);
    } else {
      return parseFloat(this.state.supplierProfile.materials.MLP.quantity/1000).toFixed(2);
    }
  }
  
  renderQuality() {
    if (this.state.supplierProfile.materials.PET.quality === 0) {
      return "-";
    } else {
      if (this.props.match.params.material === "PET") {
        return this.state.supplierProfile.materials.PET.quality;
      } else {
        return this.state.supplierProfile.materials.MLP.quality;
      }
    }
  }
  
  renderRating() {
    if (this.state.supplierProfile.materials.PET.quality === 0) {
      return "-";
    } else {
      if (this.props.match.params.material === "PET") {
        return parseFloat(this.state.supplierProfile.materials.PET.quality/20).toFixed(2);
      } else {
        return parseFloat(this.state.supplierProfile.materials.MLP.quality/20).toFixed(2);
      }
    }
  }
  
  renderPercentage() {
    if (this.state.supplierProfile.materials.PET.quality !== 0) {
      return <span className="unit"> %</span>;
    } else return null;
  }
  
  renderAveragePrice = () => {
    if (this.props.match.params.material === "PET") {
      return this.state.supplierProfile.materials.PET.average;
    } else {
      return this.state.supplierProfile.materials.MLP.average;
    }
  }
  
  render() {
    return (
      <div>
        <Topbar />
        <div className="theme-light container__wrap">
          <Row className="align-items-top">
            <Col sm={6} md={4} className="u-sticky">
              <Card>
                <CardBody>
                  <div className="Profile-header">Profile</div>
                  <div className="profile-image-container">
                    {
                      this.state.supplierProfile ? (
                        <img src={this.state.supplierProfile.image} alt="ProfileImage" />
                      ) : null
                    }
                  </div>
                  <div className="Profile-container">
                    <span className="Profile-header-sub">Name </span>
                    <span className="Profile-desc">{this.state.supplierProfile ? this.state.supplierProfile.name : null}</span>
                  </div>
                  <div className="Profile-container">
                    <span className="Profile-header-sub">User ID </span>
                    <span className="Profile-desc">{this.state.supplierProfile ? this.state.supplierProfile.user_id : null}</span>
                  </div>
                  <div className="Profile-container">
                    <span className="Profile-header-sub">Category </span>
                    <span className="Profile-desc">{this.state.supplierProfile ? this.state.supplierProfile.category : null}</span>
                  </div>
                  <div className="Profile-container">
                    <span className="Profile-header-sub">Address </span>
                    <span className="Profile-desc">{this.state.supplierProfile ? this.state.supplierProfile.address : null}</span>
                  </div>
                  <div className="Profile-container">
                    <span className="Profile-header-sub">Mobile </span>
                    <span className="Profile-desc">{this.state.supplierProfile ? this.state.supplierProfile.mobile : null}</span>
                  </div>
                  <div className="Profile-container">
                    <span className="Profile-header-sub">Government Id Type </span>
                    <span className="Profile-desc">{this.state.supplierProfile ? this.state.supplierProfile.gov_id_type : null}</span>
                  </div>
                  <div className="Profile-container">
                    <span className="Profile-header-sub">Government Id </span>
                    <span className="Profile-desc">{this.state.supplierProfile ? this.state.supplierProfile.gov_id : null}</span>
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col sm={6} md={8}>
              <Row>
                <Col sm={12} md={4}>
                  <Card>
                    <CardBody className="dashboard__card-widget">
                      <div className="card__title">
                        <h5>RECOVERED VOLUME</h5>
                      </div>
                      <div className="dashboard__total dashboard__total--area">
                        <div className="dashboard__total-stat">
                          {this.state.supplierProfile ? this.renderQuantity() : null}
                          <span className="unit"> TONS</span>
                        </div>
                      </div>
                    </CardBody>
                  </Card>
                </Col>
                <Col sm={12} md={4}>
                  <Card>
                    <CardBody className="dashboard__card-widget">
                      <div className="card__title">
                        <h5>MATERIAL QUALITY</h5>
                      </div>
                      <div className="dashboard__total dashboard__total--area">
                        <div className="dashboard__total-stat">
                          {this.state.supplierProfile ? this.renderQuality() : null}                          
                          {this.state.supplierProfile ? this.renderPercentage() : null}
                        </div>
                      </div>
                    </CardBody>
                  </Card>
                </Col>
                <Col sm={12} md={4}>
                  <Card>
                    <CardBody className="dashboard__card-widget">
                      <div className="card__title">
                        <h5>TOTAL ORDERS PLACED</h5>
                      </div>
                      <div className="dashboard__total dashboard__total--area">
                        <div className="dashboard__total-stat">
                          {this.state.supplierProfile ? this.state.supplierProfile.orders_placed : null}
                        </div>
                      </div>
                    </CardBody>
                  </Card>
                </Col>
              </Row> 
              <Row>
                <Col sm={12} md={4}>
                  <Card>
                    <CardBody className="dashboard__card-widget">
                      <div className="card__title">
                        <h5>AVERAGE KC PRICE</h5>
                      </div>
                      <div className="dashboard__total dashboard__total--area">
                        <div className="dashboard__total-stat">
                          {this.state.supplierProfile ? this.renderAveragePrice() : null}
                          <span className="unit"> INR / Kg</span>
                          {/* <span className="unit__desc">
                            <span className="profilecard__link"><a target="_blank" rel="noopener noreferrer" href="https://drive.google.com/open?id=1miZqmqULnM0GzZG0vqdivfHBP2HlsKUv">View KC Purchase Vouchers</a></span>
                          </span> */}
                        </div>
                      </div>
                    </CardBody>
                  </Card>
                </Col>
                <Col sm={12} md={4}>
                  <Card>
                    <CardBody className="dashboard__card-widget">
                      <div className="card__title">
                        <h5>WORKING CAPITAL PROVIDED</h5>
                      </div>
                      <div className="dashboard__total dashboard__total--area">
                        <div className="dashboard__total-stat">
                          {this.state.supplierProfile ? this.state.supplierProfile.working_capital : null}
                          <span className="unit"> INR</span>
                          {/* <span className="profilecard__link"><a target="_blank" rel="noopener noreferrer" href="https://drive.google.com/open?id=1miZqmqULnM0GzZG0vqdivfHBP2HlsKUv">View Working Capital Vouchers</a></span> */}
                        </div>
                      </div>
                    </CardBody>
                  </Card>
                </Col>
                <Col sm={12} md={4}>
                  <Card>
                    <CardBody className="dashboard__card-widget">
                      <div className="card__title">
                        <h5>KC Rating</h5>
                      </div>
                      <div className="dashboard__total dashboard__total--area">
                        <div className="dashboard__total-stat">
                          {this.state.supplierProfile ? this.renderRating() : null}
                          <span className="unit"> / 5</span>
                        </div>
                      </div>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
              <Row>
                {
                  this.state.supplierProfile ? (
                    <SupplierProfileMap 
                      supplierLatitude={this.state.supplierProfile.latitude} 
                      supplierLongitude={this.state.supplierProfile.longitude}
                      name={this.state.supplierProfile.name} 
                    />
                  ) : null
                }
              </Row> 
              <Row>
                <Col sm={12}>
                  {
                    this.state.supplierProfile ? (
                      <OrderTable orders={this.state.supplierProfile.orders} material={this.props.match.params.material} />
                    ) : null
                  }
                </Col>
                
              </Row>      
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

Profile.propTypes = {
  match: propTypes.object,
};

Profile.defaultProps = {
  match: {}
};

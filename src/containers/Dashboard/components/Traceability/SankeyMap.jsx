import React, { PureComponent } from 'react';
import { Sankey, Hint } from 'react-vis';
import { Col } from 'reactstrap';
import propTypes from 'prop-types';

const BLURRED_LINK_OPACITY = 0.3;
const FOCUSED_LINK_OPACITY = 0.8;

class SankeyMap extends PureComponent {
  state = {
    activeLink: null,
    width: 0
  };
  
  componentDidMount() {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions.bind(this));
  }
  
  updateDimensions() {
    this.setState({ width: window.innerWidth });
  }

  renderHint() {
    const { activeLink } = this.state;

    // calculate center x,y position of link for positioning of hint
    const x =
      activeLink.source.x1 + (activeLink.target.x0 - activeLink.source.x1) / 16;
    const y = activeLink.y0 - (activeLink.y0 - activeLink.y1) / 16;

    const hintValue = {
      [`${activeLink.source.name} ➞ ${
        activeLink.target.name
      }`]: activeLink.value + " Kg"
    };

    return (
      <Hint 
        x={x} 
        y={y} 
        value={hintValue}
        innerWidth={100} 
        style={{ 
          value: { 
            paddingLeft: 3, paddingRight: 3, paddingTop: 6, paddingBottom: 6, fontFamily: "'Open Sans', sans-serif", fontWeight: 600 
          },
          title: {
            paddingLeft: 3, paddingRight: 3, paddingTop: 6, paddingBottom: 6, fontFamily: "'Open Sans', sans-serif", fontWeight: 600 
          } 
        }} 
      />
    );
  }
  
  renderWidth() {
    if (window.innerWidth > 760 && window.innerWidth < 1200) {
      return 540;
    } else if (window.innerWidth > 400 && window.innerWidth < 750) {
      return 401;
    } else {
      return window.innerWidth - 20;
    }
  }

  render() {
    console.log('sankey', this.props.data);
    const { data } = this.props;
    const { activeLink } = this.state;
    const nodes = data.nodes;
    const links = data.links;
    return (
      <Col md={12} xl={3} lg={6} xs={12}>
        <div style={{ marginLeft: -18 }}>
          <Sankey
            nodes={nodes.map(d => ({ ...d }))}
            links={links.map((d, i) => ({
              ...d,
              opacity:
                activeLink && i === activeLink.index
                  ? FOCUSED_LINK_OPACITY
                  : BLURRED_LINK_OPACITY
            }))}
            animation
            width={this.renderWidth()}
            height={650}
            onLinkMouseOver={node => this.setState({ activeLink: node })}
            onLinkMouseOut={() => this.setState({ activeLink: null })}
            style={{
                links: {
                  opacity: 0.6,
                },
                labels: {
                  fontSize: '0px'
                },
                rects: {
                  strokeWidth: 5,
                  stroke: '#1A3177',
                  width: 3
                }
              }}
          >
            {activeLink && this.renderHint()}
          </Sankey>
        </div>
      </Col>
    );
  }
}

SankeyMap.propTypes = {
  data: propTypes.object
};

SankeyMap.defaultProps = {
  data: {}
};

export default SankeyMap;

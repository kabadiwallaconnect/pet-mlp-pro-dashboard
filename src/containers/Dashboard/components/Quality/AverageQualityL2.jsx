import React from 'react';
import { Card, CardBody, Col } from 'reactstrap';
import propTypes from 'prop-types';

const AverageQualityL2 = (props) => {
  return (
    <Col md={12} xl={12} lg={12} xs={12}>
      <Card>
        <CardBody className="dashboard__card-widget">
          <div className="card__title">
            <h5>AVERAGE MATERIAL QUALITY OF L2</h5>
          </div>
          <div className="dashboard__total dashboard__total--area">
            <p className="dashboard__total-stat">
              {props.value}
              <span className="unit">%</span>
            </p>
          </div>
        </CardBody>
      </Card>
    </Col>
  );
};

AverageQualityL2.propTypes = {
  value: propTypes.number
};

AverageQualityL2.defaultProps = {
  value: 0
};

export default AverageQualityL2;

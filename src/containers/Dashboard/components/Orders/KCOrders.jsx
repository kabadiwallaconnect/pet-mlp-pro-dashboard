import React, { Component } from 'react';
import propTypes from 'prop-types';
import { filter } from 'fuzzaldrin-plus';
import {
  Table,
  Popover,
  Position,
  Menu,
  Avatar,
  Text,
  TextDropdownButton
} from 'evergreen-ui';
import { connect } from 'react-redux';
import Loader from '../../components/Loader';
// import Pagination from './Paginations';
import Topbar from '../../../Layout';
import history from '../../../App/History';
import { 
  currentPageStore, 
  getAllOrder,
  categoryStore 
} from '../../../../redux/actions/supplierActions';

const Order = {
  NONE: 'NONE',
  ASC: 'ASC',
  DESC: 'DESC'
};

class KCL1Suppliers extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      searchQuery: '', // Fuzzy Search query state
      orderedColumn: 2,
      ordering: Order.ASC,
      column2Show: 'Order ID' 
    };
  }
  
  componentDidMount() {
    const category = "orders";
    const materialType = this.props.match.params.material; 
    const page = Number(this.props.match.params.page);
    this.props.currentPageStore(page);
    this.props.categoryStore(category);
    this.props.getAllOrder({ materialType });
  }
  
  componentDidUpdate(prevProp) {
    if (this.props.match.params.page !== prevProp.match.params.page) {
      this.getDetails();
    }
  }
  
  onSelectRow(order) {
    const materialType = this.props.match.params.material;
    history.push('/' + materialType + "/order/details/" + order.order_id);
  }
  
  getDetails() {
    console.log("GET Order Details");
  }
  
  getIconForOrder = order => {
    switch (order) {
      case Order.ASC:
        return 'arrow-up';
      case Order.DESC:
        return 'arrow-down';
      default:
        return 'caret-down';
    }
  }
  
  sort = (allOrders) => {
    // Ordering - Type of ordering ('ASC' or 'DESC')
    // orderedColumn - Column number based on which table is sorted
    const { ordering, orderedColumn } = this.state;
    // Return if there's no ordering.
    if (ordering === Order.NONE) return allOrders;

    // Get the property to sort each profile on.
    // By default use the `name` property.
    let propKey = 'name';
    if (orderedColumn === 2) propKey = 'order_id';
    if (orderedColumn === 4) propKey = 'quantity';
    if (orderedColumn === 5) propKey = 'quality';
    if (orderedColumn === 6) propKey = 'actual_total';
    // if (orderedColumn === 7) propKey = 'distance';

    return allOrders.sort((a, b) => {
      let aValue = a[propKey];
      let bValue = b[propKey];
      aValue = Number(aValue);
      bValue = Number(bValue);

      // Support string comparison
      // Sorting conditions (-1 -> a comes first, 1 -> b comes first, 0 -> a equals b)
      const sortTable = { true: 1, false: -1 };
      
      // Order ascending (Order.ASC)
      if (this.state.ordering === Order.ASC) {
        return aValue === bValue ? 0 : sortTable[aValue > bValue];
      } else {
        // Order descending (Order.DESC)
        return bValue === aValue ? 0 : sortTable[bValue > aValue];
      }  
    });
  }
  
  // Filter the profiles based on the name property.
  filter = () => {
    const { allOrders } = this.props;
    const searchQuery = this.state.searchQuery.trim();
    console.log('searchQuery', searchQuery);
    // If the searchQuery is empty, return the profiles as is.
    if (searchQuery.length === 0) return allOrders;

    return allOrders.filter(order => {
      console.log('order', order);
      const result = filter([order.order_id, order.name], searchQuery);
      console.log('result', result);
      // return result;
      console.log(result.length);
      return result.length === 1;
    });
  }
  
  handleFilterChange = value => {
    this.setState({ searchQuery: value });
  }
  
  handleFilterChangeValue(evt) {
    this.setState({ searchQuery: evt.target.value });
  }
  
  renderQuality(order) {
    if (order.rejects !== null) {
      return parseFloat((1 - (order.rejects/order.quantity)) * 100).toFixed(2) + '%';
    } else {
      return "-";
    }
  }
  
  // Order ID
  renderOrderTableHeaderCell = () => {
    return (
      <Table.TextHeaderCell>
        <Popover
          position={Position.BOTTOM_LEFT}
          content={({ close }) => (
            <Menu>
              <Menu.OptionsGroup
                title="Order"
                options={[
                  { label: 'Ascending', value: Order.ASC },
                  { label: 'Descending', value: Order.DESC }
                ]}
                selected={
                  this.state.orderedColumn === 2 ? this.state.ordering : null
                }
                onChange={value => {
                  console.log('filter on onChange', value);
                  this.setState({
                    orderedColumn: 2,
                    ordering: value
                  });
                  // Close the popover when you select a value.
                  close();
                }}
              />
            </Menu>
          )}
        >
          <TextDropdownButton
            icon={
              this.state.orderedColumn === 2
                ? this.getIconForOrder(this.state.ordering)
                : 'caret-down'
            }
          >
            <div className="headerCell">Order ID</div>
          </TextDropdownButton>
        </Popover>
      </Table.TextHeaderCell>
    );
  }
  
  // Name
  renderNameTableHeaderCell = () => {
    return (
      <Table.HeaderCell>
        <div className="headerCell">Name</div>
      </Table.HeaderCell>
    );
  }

  // Volume
  renderVolumeTableHeaderCell = () => {
    return (
      <Table.TextHeaderCell>
        <Popover
          position={Position.BOTTOM_LEFT}
          content={({ close }) => (
            <Menu>
              <Menu.OptionsGroup
                title="Order"
                options={[
                  { label: 'Ascending', value: Order.ASC },
                  { label: 'Descending', value: Order.DESC }
                ]}
                selected={
                  this.state.orderedColumn === 4 ? this.state.ordering : null
                }
                onChange={value => {
                  console.log('filter on onChange', value);
                  this.setState({
                    orderedColumn: 4,
                    ordering: value
                  });
                  // Close the popover when you select a value.
                  close();
                }}
              />
            </Menu>
          )}
        >
          <TextDropdownButton
            icon={
              this.state.orderedColumn === 4
                ? this.getIconForOrder(this.state.ordering)
                : 'caret-down'
            }
          >
            <div className="headerCell">Volume (Kg)</div>
          </TextDropdownButton>
        </Popover>
      </Table.TextHeaderCell>
    );
  }
  
  // Quality
  renderQualityTableHeaderCell = () => {
    return (
      <Table.TextHeaderCell>
        <Popover
          position={Position.BOTTOM_LEFT}
          content={({ close }) => (
            <Menu>
              <Menu.OptionsGroup
                title="Order"
                options={[
                  { label: 'Ascending', value: Order.ASC },
                  { label: 'Descending', value: Order.DESC }
                ]}
                selected={
                  this.state.orderedColumn === 5 ? this.state.ordering : null
                }
                onChange={value => {
                  this.setState({
                    orderedColumn: 5,
                    ordering: value
                  });
                  // Close the popover when you select a value.
                  close();
                }}
              />
            </Menu>
          )}
        >
          <TextDropdownButton
            icon={
              this.state.orderedColumn === 5
                ? this.getIconForOrder(this.state.ordering)
                : 'caret-down'
            }
          >
            <div className="headerCell">Quality</div>
          </TextDropdownButton>
        </Popover>
      </Table.TextHeaderCell>
    );
  }
  
  // Amount
  renderAmountTableHeaderCell = () => {
    return (
      <Table.TextHeaderCell>
        <Popover
          position={Position.BOTTOM_LEFT}
          content={({ close }) => (
            <Menu>
              <Menu.OptionsGroup
                title="Order"
                options={[
                  { label: 'Ascending', value: Order.ASC },
                  { label: 'Descending', value: Order.DESC }
                ]}
                selected={
                  this.state.orderedColumn === 6 ? this.state.ordering : null
                }
                onChange={value => {
                  this.setState({
                    orderedColumn: 6,
                    ordering: value
                  });
                  // Close the popover when you select a value.
                  close();
                }}
              />
            </Menu>
          )}
        >
          <TextDropdownButton
            icon={
              this.state.orderedColumn === 6
                ? this.getIconForOrder(this.state.ordering)
                : 'caret-down'
            }
          >
            <div className="headerCell">Amount</div>
          </TextDropdownButton>
        </Popover>
      </Table.TextHeaderCell>
    );
  }
  
  // Distance
  // renderDistanceTableHeaderCell = () => {
  //   return (
  //     <Table.TextHeaderCell>
  //       <Popover
  //         position={Position.BOTTOM_LEFT}
  //         content={({ close }) => (
  //           <Menu>
  //             <Menu.OptionsGroup
  //               title="Order"
  //               options={[
  //                 { label: 'Ascending', value: Order.ASC },
  //                 { label: 'Descending', value: Order.DESC }
  //               ]}
  //               selected={
  //                 this.state.orderedColumn === 7 ? this.state.ordering : null
  //               }
  //               onChange={value => {
  //                 this.setState({
  //                   orderedColumn: 7,
  //                   ordering: value
  //                 });
  //                 // Close the popover when you select a value.
  //                 close();
  //               }}
  //             />
  //           </Menu>
  //         )}
  //       >
  //         <TextDropdownButton
  //           icon={
  //             this.state.orderedColumn === 7
  //               ? this.getIconForOrder(this.state.ordering)
  //               : 'caret-down'
  //           }
  //         >
  //           <div className="headerCell">Distance</div>
  //         </TextDropdownButton>
  //       </Popover>
  //     </Table.TextHeaderCell>
  //   );
  // }
  
  // Table Row Values
  renderRow = ({ order }) => {
    console.log('order', order);
    return (
      <Table.Row key={order.order_id} isSelectable onSelect={() => this.onSelectRow(order)}>
        <Table.Cell flexBasis={560} flexShrink={0} flexGrow={0} display="flex" alignItems="center">
          <Avatar name={order.name} />
          <Text marginLeft={8} size={300} fontWeight={500}>
            {order.order_id}
          </Text>
        </Table.Cell>
        <Table.TextCell flexBasis={560} flexShrink={0} flexGrow={0} className="table__font"><Text fontFamily={"'Open Sans', sans-serif"} fontSize={12}>{order.name}</Text></Table.TextCell>
        <Table.TextCell isNumber flexBasis={560} flexShrink={0} flexGrow={0} className="table__font"><Text fontFamily={"'Open Sans', sans-serif"} fontSize={12}>{order.quantity ? parseFloat(order.quantity).toFixed(2) : null}</Text></Table.TextCell>
        <Table.TextCell isNumber flexBasis={560} flexShrink={0} flexGrow={0} className="table__font"><Text fontFamily={"'Open Sans', sans-serif"} fontSize={12}>{this.renderQuality(order)}</Text></Table.TextCell>
        <Table.TextCell isNumber flexBasis={560} flexShrink={0} flexGrow={0} className="table__font"><Text fontFamily={"'Open Sans', sans-serif"} fontSize={12}>Rs. {(order.actual_total)}</Text></Table.TextCell>
        {/* <Table.TextCell isNumber flexBasis={560} flexShrink={0} flexGrow={0} className="table__font"><Text fontFamily={"'Open Sans', sans-serif"} fontSize={12}>{(order.distance)}</Text></Table.TextCell> */}
      </Table.Row>
    );
  }


  render() {
    const { allOrdersLoading, allOrders } = this.props;
    const items = this.filter(this.sort(allOrders));
    return (
      <div>
        <Topbar />
        {
          allOrdersLoading ? (
            <Loader load={allOrdersLoading} />
          ) : (
            <div className="table__container__wrap table__container">
              {/* <div className="searchTable__search-container">
                <div className="searchTable__search-text">Search</div>
                <input 
                  className="searchTable__search-input"
                  onChange={(evt) => this.handleFilterChangeValue(evt)}
                  value={this.state.searchQuery}
                />
              </div> */}
              <Table border>
                <Table.Head intent="success">
                  {this.renderOrderTableHeaderCell()}
                  {this.renderNameTableHeaderCell()}
                  {this.renderVolumeTableHeaderCell()}
                  {this.renderQualityTableHeaderCell()}
                  {this.renderAmountTableHeaderCell()}
                  {/* {this.renderDistanceTableHeaderCell()} */}
                </Table.Head>
                <Table.VirtualBody height={window.innerHeight - 150}>
                  {items.map((item, index) => this.renderRow({ order: item, index }))}
                </Table.VirtualBody>
              </Table>
            </div>
          )
        }
        {/* <Pagination 
          material={this.props.match.params.material}
          category={this.props.category}
        />         */}
      </div>
    );
  }
}

KCL1Suppliers.propTypes = {
  match: propTypes.object,
  currentPageStore: propTypes.func.isRequired,
  categoryStore: propTypes.func.isRequired,
  // category: propTypes.string.isRequired,
  getAllOrder: propTypes.func.isRequired,
  allOrdersLoading: propTypes.bool.isRequired,
  allOrders: propTypes.array.isRequired
};

KCL1Suppliers.defaultProps = {
  match: {}
};

const mapStateToProps = (state) => {
  return {
    allOrdersLoading: state.supplier.allOrdersLoading,
    allOrders: state.supplier.allOrders,
    category: state.supplier.category
  };
};

export default connect(
  mapStateToProps,
  {
    currentPageStore,
    getAllOrder,
    categoryStore
  }
)(KCL1Suppliers);

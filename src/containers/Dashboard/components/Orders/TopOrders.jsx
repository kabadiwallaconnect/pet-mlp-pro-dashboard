import React from 'react';
import { Card, CardBody, Table } from 'reactstrap';
// import { translate } from 'react-i18next';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import { Link } from 'react-router-dom';

const TopOrders = (props) => (
  <Card>
    <CardBody>
      <div className="card__title card__titleMargin card__title--row">
        <h5>RECENT ORDERS</h5>
        <div><Link className="button" to={{ pathname: "/" + props.material + "/orders" }}>View All</Link></div>
      </div>
      <Table className="table--bordered table--head-accent" responsive striped>
        <thead>
          <tr>
            <th>Order ID</th>
            <th>Date</th>
            <th>Name</th>
            <th>Volume (Kg)</th>
            <th>Quality</th>
            <th>Amount</th>
          </tr>
        </thead>
        <tbody>
          {
            props.topOrders.map((order) => {
              return (
                <tr key={Number(order.order_id)}>
                  <td><Link to={{ pathname: "/" + props.material + "/order/details/" + order.order_id, state: { message: props.material } }}>{order.order_id}</Link></td>
                  <td>{order.date}</td>
                  <td>{order.name}</td>
                  <td>{parseFloat(order.quantity).toFixed(2)}</td>
                  <td>{parseFloat((1 - (order.rejects/order.quantity)) * 100).toFixed(2)}%</td>
                  <td>Rs. {parseFloat(order.actual_total).toFixed(2)}</td>
                </tr>
              ); 
            })
          }
        </tbody>
      </Table>
    </CardBody>
  </Card>  
);

TopOrders.propTypes = {
  topOrders: propTypes.array,
  material: propTypes.string.isRequired
};

TopOrders.defaultProps = {
  topOrders: []
};

const mapStateToProps = () => {
  return {
    
  };
};

export default connect(
  mapStateToProps, 
  {
    
  }
)(TopOrders);
// export default translate('common')(HeadAccentTable);

import React from 'react';
import { Card, CardBody, Table } from 'reactstrap';
// import { translate } from 'react-i18next';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
// import { Link } from 'react-router-dom';

const TopOrders = (props) => (
  <Card>
    <CardBody>
      <Table className="table--bordered table--head-accent" responsive striped>
        <thead>
          <tr>
            <th>Material</th>
            <th>Volume</th>
            <th>Price / Kg</th>
            <th>Amount</th>
          </tr>
        </thead>
        <tbody>
          {
            props.data.map((value, index) => {
              return (
                <tr key={Number(index)}>
                  <td>{value.material}</td>
                  <td>{parseFloat(value.quantity).toFixed(2)} Kg</td>
                  <td>Rs. {parseFloat(value.price).toFixed(2)}</td>
                  <td>Rs. {parseFloat(value.quantity * value.price).toFixed(2)}</td>
                </tr>
              ); 
            })
          }
        </tbody>
      </Table>
    </CardBody>
  </Card>  
);

TopOrders.propTypes = {
  data: propTypes.array,
  // material: propTypes.string.isRequired
};

TopOrders.defaultProps = {
  data: []
};

const mapStateToProps = () => {
  return {
    
  };
};

export default connect(
  mapStateToProps, 
  {
    
  }
)(TopOrders);
// export default translate('common')(HeadAccentTable);

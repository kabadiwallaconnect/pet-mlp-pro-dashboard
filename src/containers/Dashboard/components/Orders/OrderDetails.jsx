import React, { Component } from 'react';
import { Card, CardBody, Row, Col } from 'reactstrap';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import Loader from '../../components/Loader';
import Topbar from '../../../Layout';
import SegregationTable from './SegregationTable';
import BillTable from './BillTable';

import { 
  getOrderDetails
} from '../../../../redux/actions/supplierActions';

class OrderDetails extends Component {
  componentDidMount() {
    const id = this.props.match.params.id;
    const materialType = this.props.match.params.material;
    this.props.getOrderDetails({ id, materialType });
  }
  
  render() {
    const { orderDetailsLoading, orderDetails } = this.props;
    return (
      <div>
        <Topbar />
        {
          orderDetailsLoading ? (
            <Loader load={orderDetailsLoading} />
          ) : (
            <div className="theme-light container__wrap">
              <Row>
                <Col md={3} className="u-sticky">
                  <Card>
                    <CardBody>
                      <div>
                        <div className="Profile-header">Profile</div>
                        <div className="profile-image-container">
                          {
                            orderDetails.user ? (
                              <img src={orderDetails.user.image} alt="ProfileImage" />
                            ) : null
                          }
                        </div>
                      </div>
                      <div className="Profile-container">
                        <span className="Profile-header-sub">Name</span>
                        <span className="Profile-desc">{orderDetails.user ? orderDetails.user.name : null}</span>
                      </div>
                      <div className="Profile-container">
                        <span className="Profile-header-sub">User ID </span>
                        <span className="Profile-desc">{orderDetails.user ? orderDetails.user.user_id : null}</span>
                      </div>
                      <div className="Profile-container">
                        <span className="Profile-header-sub">Category</span>
                        <span className="Profile-desc">{orderDetails.user ? orderDetails.user.category : null}</span>
                      </div>
                      <div className="Profile-container">
                        <span className="Profile-header-sub">Address</span>
                        <span className="Profile-desc">{orderDetails.user ? orderDetails.user.address : null}</span>
                      </div>
                      <div className="Profile-container">
                        <span className="Profile-header-sub">Mobile</span>
                        <span className="Profile-desc">{orderDetails.user ? orderDetails.user.mobile : null}</span>
                      </div>
                      <div className="Profile-container">
                        <span className="Profile-header-sub">Government Id Type</span>
                        <span className="Profile-desc">{orderDetails.user ? orderDetails.user.gov_id_type : null}</span>
                      </div>
                      <div className="Profile-container">
                        <span className="Profile-header-sub">Government Id</span>
                        <span className="Profile-desc">{orderDetails.user ? orderDetails.user.gov_id : null}</span>
                      </div>
                    </CardBody>
                  </Card>
                </Col>
                <Col md={5}>
                  <div className="profile-card">
                    <div className="table-heading">Order Details - #{orderDetails ? orderDetails.order_id : null}</div>
                    <BillTable 
                      data={orderDetails.order_details}
                    />
                  </div>
                </Col>
                <Col md={4}>
                  <div className="profile-card">
                    <div className="table-heading">Segregation Details</div>
                    <SegregationTable 
                      data={orderDetails.segregation_details}
                    />
                  </div>
                </Col>
              </Row>
            </div>
          )
        }  
      </div>
    );
  }
}

OrderDetails.propTypes = {
  getOrderDetails: propTypes.func.isRequired,
  match: propTypes.object.isRequired,
  orderDetails: propTypes.object.isRequired,
  orderDetailsLoading: propTypes.bool.isRequired
};

OrderDetails.defaultProps = {
  
};

const mapStateToProps = (state) => {
  console.log(state.supplier);
  return {
    orderDetails: state.supplier.orderDetails,
    orderDetailsLoading: state.supplier.orderDetailsLoading
  };
};

export default connect(
  mapStateToProps, 
  {
    getOrderDetails
  }
)(OrderDetails);

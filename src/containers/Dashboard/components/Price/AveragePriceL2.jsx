import React from 'react';
import { Card, CardBody, Col } from 'reactstrap';
import propTypes from 'prop-types';

const AveragePriceL2 = (props) => {
  return (
    <Col md={12} xl={12} lg={12} xs={12}>
      <Card>
        <CardBody className="dashboard__card-widget-large">
          <div className="card__title">
            <h5>AVERAGE KC BUYING PRICE FOR L2</h5>
          </div>
          <div className="dashboard__total dashboard__total--area">
            <p className="dashboard__total-stat">
              {props.value}
              <span className="unit"> INR / Kg</span>
              {
                props.material === "PET" ? (
                  <span className="unit__desc unit__desc--block"> Market Price + 2 INR
                    <span className="card__link"><a target="_blank" rel="noopener noreferrer" href="https://drive.google.com/file/d/1l_vFfKLWW1kyzI1nNyDBGFqZjpuydq2_/view?usp=sharing">View Sample Voucher</a></span>
                  </span>
                ) : null
              }
            </p>
          </div>
        </CardBody>
      </Card>
    </Col>
  );
};

AveragePriceL2.propTypes = {
  value: propTypes.number,
  material: propTypes.string.isRequired
};

AveragePriceL2.defaultProps = {
  value: 0
};

export default AveragePriceL2;

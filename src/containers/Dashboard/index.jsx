import React, { Component } from 'react';
import { Col, Container, Row, DropdownMenu, DropdownItem, DropdownToggle, UncontrolledDropdown } from 'reactstrap';
import ChevronDownIcon from 'mdi-react/ChevronDownIcon';
import axios from 'axios';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import moment from 'moment';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import 'bootstrap-daterangepicker/daterangepicker.css';
import Volume from './components/Volume/Volume';
import Recycled from './components/Volume/Recycled';
import RejectsVolume from './components/Volume/RejectsVolume';

import L1Volume from './components/Volume/L1Volume';
import L2Volume from './components/Volume/L2Volume';
import L1VolumeAvg from './components/Volume/L1VolumeAvg';
import L2VolumeAvg from './components/Volume/L2VolumeAvg';

import TotalSuppliers from './components/Suppliers/TotalSuppliersCount';
import L1Suppliers from './components/Suppliers/L1SuppliersCount';
import L2Suppliers from './components/Suppliers/L2SuppliersCount';
import TopL1Suppliers from './components/Suppliers/TopL1Suppliers';
import TopL2Suppliers from './components/Suppliers/TopL2Suppliers';
import TopOrders from './components/Orders/TopOrders';

import SankeyMap from './components/Traceability/SankeyMap';
import SupplierPrice from './components/Suppliers/SupplierPriceLineGraph';
import AveragePriceL1 from './components/Price/AveragePriceL1';
import AveragePriceL2 from './components/Price/AveragePriceL2';
import AverageQualityL1 from './components/Quality/AverageQualityL1';
import AverageQualityL2 from './components/Quality/AverageQualityL2';

import ProcurementCost from './components/Cost/ProcurementCost';
import HRCost from './components/Cost/HRCost';
import OperationCost from './components/Cost/OperationCost';
import TransportationCost from './components/Cost/TransportationCost';

import SupplierMap from './components/Suppliers/SupplierMap';
import calendarIcon from '../../shared/img/calendar.svg';
import Loader from './components/Loader';

import getVolume from '../../redux/actions/volumeActions';
import getCost from '../../redux/actions/costActions';
import { getSupplierMap, getSuppliers, getSupplierPrice, getSupplierList, getTopOrders } from '../../redux/actions/supplierActions';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      data: '',
      sankeyData: '',
      loading: true,
      materialType: this.props.match.params.material,
      startDate: this.props.match.params.material === "PET" ? moment("2016-04-13", "YYYY-MM-DD") : moment("2018-08-21", "YYYY-MM-DD"),
      endDate: moment("2019-04-06", "YYYY-MM-DD"),
      ranges: {
        'This Week': [moment().startOf('week'), moment().endOf('week')],
        'Last Week': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
        'Week Before the Last': [moment().subtract(2, 'week').startOf('week'), moment().subtract(2, 'week').endOf('week')]
      } 
    };
  }
    
  componentDidMount() {
    console.log(this.props);
    const { materialType } = this.state;
    const startVal = this.state.startDate.format('YYYY-MM-DD').toString();
    const endVal = this.state.endDate.format('YYYY-MM-DD').toString();
    // Sankey map API Call
    axios.get('https://pro-api.kcdev.xyz/public/graph/volume/flow', {
      params: {
        start: startVal,
        end: endVal,
        material: this.state.materialType
      }
    }).then(res => {
      this.setState({ sankeyData: res.data.data, loading: false });
    });
    // volume
    this.props.getVolume({ startVal, endVal, materialType });
    // Cost
    this.props.getCost({ startVal, endVal, materialType });
    // Supplier Map
    this.props.getSupplierMap({ startVal, endVal, materialType });
    // Suppliers 
    this.props.getSuppliers({ startVal, endVal, materialType });
    // Suppliers Price
    this.props.getSupplierPrice({ startVal, endVal, materialType });
    // Suppliers List
    this.props.getSupplierList({ startVal, endVal, materialType });
    // Top Orders 
    this.props.getTopOrders({ materialType });
  }
  
  componentDidUpdate(prevProps) {
    if (this.props.match.params.material !== prevProps.match.params.material) {
      this.handlePropChange(this.props.match.params.material);
    }  
  }
  
  handlePropChange(materialType) {
    this.setState({ materialType });
    if (materialType === "MLP") {
      this.setState({ startDate: moment("2018-08-21", "YYYY-MM-DD") });
      this.setState({ endDate: moment("2019-04-04", "YYYY-MM-DD") });
    } else {
      this.setState({ startDate: moment("2016-04-13", "YYYY-MM-DD") });
      this.setState({ endDate: moment("2019-01-01", "YYYY-MM-DD") });
    }
    this.setState((state) => {
      const startVal = state.startDate.format('YYYY-MM-DD').toString();
      const endVal = state.endDate.format('YYYY-MM-DD').toString();
      // volume
      this.props.getVolume({ startVal, endVal, materialType });
      // Cost
      this.props.getCost({ startVal, endVal, materialType });
      // Supplier Map
      this.props.getSupplierMap({ startVal, endVal, materialType });
      // Suppliers 
      this.props.getSuppliers({ startVal, endVal, materialType });
      // Suppliers Price
      this.props.getSupplierPrice({ startVal, endVal, materialType });
      // Suppliers List
      this.props.getSupplierList({ startVal, endVal, materialType });
      // Top Orders 
      this.props.getTopOrders({ materialType });
    });
  }
  
  // Custom Date Change
  handleApply = (event, picker) => {
    const { materialType } = this.state;
    this.setState({
      startDate: picker.startDate,
      endDate: picker.endDate,
      loading: true
    });
    const startVal = this.state.startDate.format('YYYY-MM-DD').toString();
    const endVal = this.state.endDate.format('YYYY-MM-DD').toString();
    // Sankey
    axios.get('https://pro-api.kcdev.xyz/public/graph/volume/flow', {
      params: {
        start: startVal,
        end: endVal,
        material: this.state.materialType
      }
    }).then(res => {
      this.setState({ sankeyData: res.data.traceablity, loading: false });
    });
    // Volume   
    this.props.getVolume({ startVal, endVal, materialType });
    // Cost
    this.props.getCost({ startVal, endVal, materialType });
    // Supplier Map
    this.props.getSupplierMap({ startVal, endVal, materialType });
    // Suppliers 
    this.props.getSuppliers({ startVal, endVal, materialType });
    // Suppliers Price
    this.props.getSupplierPrice({ startVal, endVal, materialType });
    // Suppliers List
    this.props.getSupplierList({ startVal, endVal, materialType });
    // Top Orders 
    this.props.getTopOrders({ materialType });
  }
  
  // Material Change Handler
  handleMaterialChange(material) {
    this.setState({ materialType: material });
  }
  
  renderLabel() {
    const startVal = this.state.startDate.format('DD-MM-YYYY').toString();
    const endVal = this.state.endDate.format('DD-MM-YYYY').toString();
    let label = startVal + '  to  ' + endVal;
    if (startVal === endVal) {
      label = startVal;
    }
    return label;
  }
  
  render() {  
    return (
      <Container className="dashboard">
        {
          this.props.volumeLoading ? (
            <Loader load={this.props.volumeLoading} />
          ) : (
            <div>
              <Row>
                <Col md={8} sm={4} xs={4}>
                  <div className="page-title bold-text">{this.state.materialType} Dashboard</div>
                </Col>
                <Col className="cont" md={4} sm={8} xs={8}>
                  <Row className="datePickerContainer">
                    <div>
                      <UncontrolledDropdown>
                        <DropdownToggle className="icon icon--right">
                          <p>{this.state.materialType}<ChevronDownIcon /></p>
                        </DropdownToggle>
                        <DropdownMenu className="dropdown__menu">
                          <Link to={{ pathname: '/PET' }}><DropdownItem onClick={() => this.handleMaterialChange('PET')}>PET</DropdownItem></Link>
                          <DropdownItem divider />                          
                          <Link to={{ pathname: '/MLP' }}><DropdownItem onClick={() => this.handleMaterialChange('MLP')}>MLP</DropdownItem></Link>
                        </DropdownMenu>
                      </UncontrolledDropdown>
                    </div>
                    <div>
                      <DateRangePicker
                        ranges={this.state.ranges}
                        startDate={this.state.startDate}
                        endDate={this.state.endDate}
                        onApply={this.handleApply}
                        opens="left"
                      >
                        <div className="input-group">
                          <input type="text" className="form-control" value={this.renderLabel()} readOnly />
                          <span className="input-group-btn">
                            <div className="imgContainer">
                              <img src={calendarIcon} alt="img" height="20" />
                            </div>
                          </span>
                        </div>
                      </DateRangePicker>
                    </div>
                  </Row>
                </Col>         
              </Row>
              {/* Volume */}
              <Row>
                <Col md={12}>
                  <div className="page-title bold-text">Volume</div>
                </Col>
              </Row>
              <Row>
                <Volume value={this.props.volume} />
                <Recycled value={this.props.volume.total_recycled} material={this.state.materialType} />
                <RejectsVolume value={this.props.volume.rejects} material={this.state.materialType} />
              </Row>
              <Row>
                <L1Volume value={this.props.volume} /> 
                <L2Volume value={this.props.volume} />
                <L1VolumeAvg value={this.props.volume.l1_average} />
                <L2VolumeAvg value={this.props.volume.l2_average} />
              </Row>
              <Row>
                {
                  this.state.sankeyData ? (
                    <SankeyMap 
                      data={this.state.sankeyData ? this.state.sankeyData : null} 
                    />
                  ) : null
                }
              </Row>
              {/* Supplier Map */}
              <Row>
                {
                  this.props.supplierMap ? (
                    <SupplierMap 
                      locationsPoints={this.props.supplierMap ? this.props.supplierMap : null} 
                    />
                  ) : null
                }
              </Row>
              {/* Cost */}
              <Row>
                <Col md={12}>
                  <div className="page-title bold-text">Financials</div>
                </Col>
              </Row>
              <Row>
                <ProcurementCost value={this.props.cost.purchase_amount} />
                <TransportationCost value={this.props.cost.transport} />
                <OperationCost value={this.props.cost.operations} />
                <HRCost value={this.props.cost.sales_amount} />
              </Row>
              {/* Suppliers */}
              <Row>
                <Col md={12}>
                  <div className="page-title bold-text">Suppliers</div>
                </Col>
              </Row>
              <Row>
                <TotalSuppliers value={this.props.suppliers ? this.props.suppliers.count.total : null} />
                <L1Suppliers value={this.props.suppliers ? this.props.suppliers.count.L1: null} />
                <L2Suppliers value={this.props.suppliers ? this.props.suppliers.count.L2: null} />
              </Row>
              <Row>
                <Col md={4}>
                  <Row>
                    <AveragePriceL1 value={this.props.suppliers ? this.props.suppliers.price.L1: null} material={this.state.materialType} />
                  </Row>
                  <Row>
                    <AveragePriceL2 value={this.props.suppliers ? this.props.suppliers.price.L2: null} material={this.state.materialType} />
                  </Row>
                  <Row>
                    <AverageQualityL1 value={this.props.suppliers ? this.props.suppliers.quality.L1: null} />
                  </Row>
                  <Row>
                    <AverageQualityL2 value={this.props.suppliers ? this.props.suppliers.quality.L2: null} />
                  </Row>
                </Col>
                <Col md={8}>
                  <Row>
                    {
                      <SupplierPrice labels={this.props.supplierPrice.labels} L1_price={this.props.supplierPrice.l1_price} L2_price={this.props.supplierPrice.l2_price} />
                    }
                  </Row>
                </Col>
              </Row>  
              <Row>
                <Col>
                  {
                    this.props.supplierList ? (
                      <TopL1Suppliers suppliers={this.props.supplierList ? this.props.supplierList.L1 : null} material={this.state.materialType} />
                    ) : null
                  }
                </Col>
                <Col>
                  {
                    this.props.supplierList ? (
                      <TopL2Suppliers suppliers={this.props.supplierList ? this.props.supplierList.L2 : null} material={this.state.materialType} />
                    ) : null
                  }
                </Col>    
              </Row> 
              <Row>
                <Col sm={12}>
                  {
                    this.props.topOrders ? (
                      <TopOrders topOrders={this.props.topOrders ? this.props.topOrders : null} material={this.state.materialType} />
                    ) : null
                  }
                </Col>
              </Row>           
            </div>
          )
        }
      </Container>
    );
  }
}

Dashboard.propTypes = {
  match: propTypes.object.isRequired,
  getVolume: propTypes.func.isRequired,
  getCost: propTypes.func.isRequired,
  getSupplierMap: propTypes.func.isRequired,
  getSuppliers: propTypes.func.isRequired,
  getSupplierPrice: propTypes.func.isRequired,
  getSupplierList: propTypes.func.isRequired,
  getTopOrders: propTypes.func.isRequired,
  volume: propTypes.object.isRequired,
  volumeLoading: propTypes.bool.isRequired,
  supplierMap: propTypes.array.isRequired,
  cost: propTypes.object.isRequired,
  suppliers: propTypes.object.isRequired,
  supplierPrice: propTypes.object.isRequired,
  supplierList: propTypes.object.isRequired,
  topOrders: propTypes.array.isRequired
};

const mapStateToProps = (state) => {
  console.log('state', state);
  return {
    volume: state.volume.volume,
    volumeLoading: state.volume.volumeLoading,
    cost: state.cost.cost,
    costLoading: state.cost.costLoading,
    supplierMap: state.supplier.supplierMap,
    supplierMapLoading: state.supplier.supplierMapLoading,
    suppliers: state.supplier.suppliers,
    suppliersLoading: state.supplier.suppliers,
    supplierPrice: state.supplier.supplierPrice,
    supplierPriceLoading: state.supplier.supplierPriceLoading,
    supplierList: state.supplier.supplierList,
    supplierListLoading: state.supplier.supplierListLoading,
    topOrders: state.supplier.topOrders
  };
};

export default connect(
  mapStateToProps,
  {
    getVolume,
    getCost,
    getSupplierMap,
    getSuppliers,
    getSupplierPrice,
    getSupplierList,
    getTopOrders
  }
)(Dashboard);

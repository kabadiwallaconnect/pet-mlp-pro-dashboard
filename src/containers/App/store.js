import { combineReducers, createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import { reducer as reduxFormReducer } from 'redux-form';
import { sidebarReducer, themeReducer, dashboardReducer, volumeReducer, costReducer, supplierReducer } from '../../redux/reducers/index';

const reducer = combineReducers({
  form: reduxFormReducer, // mounted under "form",
  theme: themeReducer,
  sidebar: sidebarReducer,
  dashboard: dashboardReducer,
  volume: volumeReducer,
  cost: costReducer,
  supplier: supplierReducer
});

const store = createStore(reducer, {}, applyMiddleware(ReduxThunk));

export default store; 

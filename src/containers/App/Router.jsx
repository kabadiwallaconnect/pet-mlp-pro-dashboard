import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Layout from '../Layout/index';
import MainWrapper from './MainWrapper';

import Error from '../Dashboard/components/Error';
import Dashboard from '../Dashboard/index';
import SupplierProfile from '../Dashboard/components/Suppliers/Profile/SupplierProfile';
import KCL1Suppliers from '../Dashboard/components/Suppliers/TotalL1Suppliers';
import KCL2Suppliers from '../Dashboard/components/Suppliers/TotalL2Suppliers';
import KCLOrders from '../Dashboard/components/Orders/KCOrders';
import OrderDetails from '../Dashboard/components/Orders/OrderDetails';

const Pages = () => (
  <Switch>
    <Route path="/:material/" component={Dashboard} />
  </Switch>
);

const wrappedRoutes = () => (
  <div>
    <Layout />
    <div className="container__wrap">
      <Route path="/:material/" component={Pages} />
    </div>
  </div>
);

const Router = () => (
  <MainWrapper>
    <main>
      <Switch>
        <Route exact path="/:material/L1" component={KCL1Suppliers} />
        <Route exact path="/:material/L2" component={KCL2Suppliers} />
        <Route exact path="/:material/orders" component={KCLOrders} />
        <Route exact path="/:material/order/details/:id" component={OrderDetails} />

        <Route exact path="/:material/profile/:id" component={SupplierProfile} />
        <Route exact path="/:material/" component={wrappedRoutes} />
        <Redirect exact path="/" to="/PET" component={wrappedRoutes} />
        <Route component={Error} />
      </Switch>  
    </main>
  </MainWrapper>
);

export default Router;

import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { hot } from 'react-hot-loader';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import '../../scss/app.scss';
import Route from './Router';
import store from './store';
import history from './History';
import ScrollToTop from './ScrollToTop';

const App = () => {
  return (
    <Provider store={store}>
      <Router onUpdate={() => window.scrollTo(0, 0)} history={history}>
        <ScrollToTop>
          <Route />
        </ScrollToTop>
      </Router>
    </Provider>
  );
};

export default hot(module)(App);
